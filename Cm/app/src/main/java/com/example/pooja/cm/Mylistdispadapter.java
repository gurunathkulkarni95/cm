package com.example.pooja.cm;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class Mylistdispadapter extends RecyclerView.Adapter<Mylistdispadapter.ProductViewHolder> {
String usn;
    private Context mCtx;
    private List<Mylistproduct> productList;

    public Mylistdispadapter(Context mCtx, List<Mylistproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.mylistdisp_list, null);
        return new ProductViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Mylistproduct product = productList.get(position);

        //loading the image
       /** Glide.with(mCtx)
                .load(product.getImage())
                .into(holder.imageView);**/

        holder.textViewTitle.setText(product.getFirstname());
        holder.textViewShortDesc.setText(product.getMidname());
        holder.textViewRating.setText(product.getLastname());
        holder.textViewPrice.setText(product.getUsn());

        holder.textViewPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usn=holder.textViewPrice.getText().toString();
                Intent intent=new Intent(mCtx,Cstudentgrid.class);
                intent.putExtra("Usna_NUmber",usn);
                intent.putExtra("coming_from_Fstudent","100");
                mCtx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {


        public TextView textViewTitle, textViewShortDesc, textViewRating, textViewPrice;
        ImageView imageView;
       // public final Context context;
        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            textViewRating = itemView.findViewById(R.id.textViewRating);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);







                mCtx = itemView.getContext();


            //imageView = itemView.findViewById(R.id.imageView);
        }

    }
}
