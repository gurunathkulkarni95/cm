package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class Studentattemptsadpter extends RecyclerView.Adapter<Studentattemptsadpter.ProductViewHolder> {
    String usn;
    private Context mCtx;
    private List<Studentatemptproduct> productList;

    public Studentattemptsadpter(Context mCtx, List<Studentatemptproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.studentattemptlist, null);
        return new ProductViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Studentatemptproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.textViewTitle.setText(product.getSubjectname());
        holder.textViewShortDesc.setText(product.getSubjectcode());
        holder.textViewRating.setText(product.getAttempts());



    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {


        public TextView textViewTitle, textViewShortDesc, textViewRating;
        ImageView imageView;
        // public final Context context;
        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            textViewRating = itemView.findViewById(R.id.textViewRating);







            mCtx = itemView.getContext();


            //imageView = itemView.findViewById(R.id.imageView);
        }

    }
}
