package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class Cocurriculardispadapter extends RecyclerView.Adapter<Cocurriculardispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<Cocurricularproduct> productList;

    public Cocurriculardispadapter(Context mCtx, List<Cocurricularproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.cocurricularlist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Cocurricularproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(product.getUsn());
        holder.t2.setText(product.getAcyear());
        holder.t3.setText(product.getSem());
        holder.t4.setText(product.getFromdate());
        holder.t5.setText(product.getTodate());
        holder.t6.setText(product.getEventname());
        holder.t7.setText(product.getHost());
        holder.t8.setText(product.getEventlevel());
        holder.t9.setText(product.getAward());
        holder.t10.setText(product.getFinancial());
        holder.t11.setText(product.getAmount());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3 = itemView.findViewById(R.id.t3);
            t4 = itemView.findViewById(R.id.t4);
            t5 = itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);
            t8 = itemView.findViewById(R.id.t8);
            t9 = itemView.findViewById(R.id.t9);
            t10 = itemView.findViewById(R.id.t10);
            t11= itemView.findViewById(R.id.t11);
            //imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
