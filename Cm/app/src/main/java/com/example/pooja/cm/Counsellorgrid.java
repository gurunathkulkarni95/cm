package com.example.pooja.cm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

import static com.example.pooja.cm.R.id.grid;

public class Counsellorgrid extends AppCompatActivity {
    GridLayout maingrid;
Config config=new Config();
    String fid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counsellorgrid);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                fid=getIntent().getStringExtra("Usna_NUmber");
                //Toast.makeText(getApplicationContext(),fid,Toast.LENGTH_SHORT).show();
            }
        }

        else{
            fid=config.fid;
        }

        //Toast.makeText(getApplicationContext(),fid,Toast.LENGTH_SHORT).show();
        maingrid = (GridLayout) findViewById(grid);
        setSingleEvent(maingrid);
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(Counsellorgrid.this, Meetingrecordadd.class);
                        startActivity(a);
                    }
                     else if (choice == 1) {
                        Intent a = new Intent(Counsellorgrid.this, Mylistdisplay.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Counsellorgrid.this, Counsellor_notice.class);
                        a.putExtra("Usna_NUmber",fid);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 3) {
                        Intent a = new Intent(Counsellorgrid.this, Meetingrecord.class);
                        a.putExtra("Usna_NUmber", fid);
                        a.putExtra("coming_from_Fstudent", "100");
                        startActivity(a);
                    }else if (choice == 4) {
                        logoutOperationa();
                    }
                }
            });
        }
    }

    private void logoutOperationa() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Counsellorgrid.this);
        builder1.setMessage("Do you want to logout now ?");
        builder1.setCancelable(true);
        builder1.setTitle("Log Out");

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent intent = new Intent(Counsellorgrid.this, MainActivity
                                .class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                });

        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog logOutDialog = builder1.create();
        logOutDialog.show();

    }
    @Override
    public void onBackPressed() {
        logoutOperationa();
    }
}

