package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Cieform extends AppCompatActivity {
    EditText subcode,sem,year,cie1,cie2,cie3,comp,assign,grade,credit;
    Button b,clear,update;
String usn;
    TextView i1;
    Config config=new Config();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cieform);
        update=(Button)findViewById(R.id.update);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                usn=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            usn=config.usn;
        }

        //usn=config.usn;
        i1=(TextView)findViewById(R.id.id3);
        subcode=(EditText)findViewById(R.id.subjectcode);
        sem=(EditText)findViewById(R.id.sem);
        year=(EditText)findViewById(R.id.acyear);
        cie1=(EditText)findViewById(R.id.cie1);
        cie2=(EditText)findViewById(R.id.cie2);
        cie3=(EditText)findViewById(R.id.cie3);
        comp=(EditText)findViewById(R.id.compen);
        assign=(EditText)findViewById(R.id.assign);
        //see=(EditText)findViewById(R.id.see);
        grade=(EditText)findViewById(R.id.grade);
        //gradept=(EditText)findViewById(R.id.gpt);
        credit=(EditText)findViewById(R.id.credits);
        b=(Button)findViewById(R.id.submitsubject);
        clear=(Button)findViewById(R.id.clear);
        i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Cieform.this,Gradepointimage.class);
                startActivity(i);

            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncReg reg = new AsyncReg(Cieform.this);
                reg.execute(usn,subcode.getText().toString(), sem.getText().toString(),year.getText().toString(),cie1.getText().toString(),cie2.getText().toString(),cie3.getText().toString(),comp.getText().toString(),assign.getText().toString()
                        ,grade.getText().toString(),credit.getText().toString());

                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                intent.putExtra("Usna_NUmber",usn);
                startActivity(intent);
                //b.setVisibility(view.GONE);
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subcode.setText("");
                sem.setText("");
                year.setText("");
                cie1.setText("");
                cie2.setText("");
                cie3.setText("");
                comp.setText("");
                assign.setText("");

                grade.setText("");

                credit.setText("");
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncReg1 reg = new AsyncReg1(Cieform.this);
                reg.execute(usn,subcode.getText().toString(), sem.getText().toString(),year.getText().toString(),cie1.getText().toString(),cie2.getText().toString(),cie3.getText().toString(),comp.getText().toString(),assign.getText().toString()
                        ,grade.getText().toString(),credit.getText().toString());
                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                intent.putExtra("Usna_NUmber",usn);
                startActivity(intent);

            }
        });

    }




    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Cieform.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/cieform.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("subcode", params[1]);
                builder.appendQueryParameter("sem", params[2]);
                builder.appendQueryParameter("year", params[3]);
                builder.appendQueryParameter("cie1", params[4]);
                builder.appendQueryParameter("cie2", params[5]);
                builder.appendQueryParameter("cie3", params[6]);
                builder.appendQueryParameter("comp", params[7]);
                builder.appendQueryParameter("assign", params[8]);
                //builder.appendQueryParameter("see", params[9]);
                builder.appendQueryParameter("grade", params[9]);
               // builder.appendQueryParameter("gradept", params[11]);
                builder.appendQueryParameter("credit", params[10]);

                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            //Toast.makeText(Cieform.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully Registered!!")) {
                Toast.makeText(Cieform.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Studentgrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Cieform.this, "Registration Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }







    class AsyncReg1 extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Cieform.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg1(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/cieformupdate.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("subcode", params[1]);
                builder.appendQueryParameter("sem", params[2]);
                builder.appendQueryParameter("year", params[3]);
                builder.appendQueryParameter("cie1", params[4]);
                builder.appendQueryParameter("cie2", params[5]);
                builder.appendQueryParameter("cie3", params[6]);
                builder.appendQueryParameter("comp", params[7]);
                builder.appendQueryParameter("assign", params[8]);
                //builder.appendQueryParameter("see", params[9]);
                builder.appendQueryParameter("grade", params[9]);
                // builder.appendQueryParameter("gradept", params[11]);
                builder.appendQueryParameter("credit", params[10]);

                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Cieform.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully Registered!!")) {
                Toast.makeText(Cieform.this, "Update Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Studentgrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Cieform.this, "Registration Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(getApplicationContext(),Studentgrid.class);
        i.putExtra("Usna_NUmber",usn);
        startActivity(i);
        finish();
    }
}
