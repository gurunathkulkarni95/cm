package com.example.pooja.cm;

/**
 * Created by gurunath on 12/04/18.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


public class Cnoticeadapter extends RecyclerView.Adapter<Cnoticeadapter.ProductViewHolder> {

Config config=new Config();

    private Context mCtx;
    private List<Cnoticeproduct> productList;

    public Cnoticeadapter(Context mCtx, List<Cnoticeproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.cnoticelist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Cnoticeproduct product = productList.get(position);

        //loading the image
        Glide.with(mCtx)
                .load(product.getImagepath())
                .into(holder.imageView);

        holder.textViewTitle.setText(product.getMessage());
        holder.textViewShortDesc.setText(product.getFname());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc, textViewPrice;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            imageView = itemView.findViewById(R.id.imageView);



        }

    }
}

