package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class Meetingrecorddispadapter extends RecyclerView.Adapter<Meetingrecorddispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<Meetingrecordproduct> productList;

    public Meetingrecorddispadapter(Context mCtx, List<Meetingrecordproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.meeting_disp, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Meetingrecordproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(product.getUsn());
        holder.t2.setText(product.getFid());
        holder.t3.setText(product.getSemester());
        holder.t4.setText(product.getAcyear());
        holder.t5.setText(product.getDate());
        holder.t6.setText(product.getPurpose());
        holder.t7.setText(product.getOutcome());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7;


        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3 = itemView.findViewById(R.id.t3);
            t4 = itemView.findViewById(R.id.t4);
            t5 = itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);

            //imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
