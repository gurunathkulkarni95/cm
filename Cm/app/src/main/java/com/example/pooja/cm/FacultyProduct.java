package com.example.pooja.cm;


public class FacultyProduct {
    private String facultyname,facultyid,designation,emailid,contact,subject,subjectcode,sem;


    public FacultyProduct(String facultyname, String facultyid, String designation,String emailid,String contact,String subject,String subjectcode,String sem)
    {
        this.facultyname= facultyname;
        this.facultyid = facultyid;
        this.designation = designation;
        this.emailid= emailid;
        this.contact= contact;
        this.subject= subject;
        this.subjectcode = subjectcode;
        this.sem = sem;
    }

    public String getFacultyname() {
        return facultyname;
    }

    public String getFacultyid() {
        return facultyid;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmailid() {
        return emailid;
    }

    public String getContact() {
        return contact;
    }

    public String getSubject() {
        return subject;
    }

    public String getSubjectcode() {
        return subjectcode;
    }

    public String getSem() {
        return sem;
    }
}

