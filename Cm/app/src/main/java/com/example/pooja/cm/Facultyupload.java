package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Facultyupload extends AppCompatActivity {
    public int PICK_IMAGE_REQUEST=1;
    Button choose,upload;
    ImageView display;
    private Bitmap bitmap;

    private Uri filePath;
    String encodedString;
    String imageName;
    Config config=new Config();
    String fid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultyupload);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                fid=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            fid=config.fid;
        }

        //Toast.makeText(getApplicationContext(),fid,Toast.LENGTH_LONG).show();
        choose = (Button) findViewById(R.id.choose);
        upload = (Button) findViewById(R.id.upload);
        display = (ImageView) findViewById(R.id.display);

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessSendPath sendpath = new ProcessSendPath(Facultyupload.this);
                sendpath.execute(fid,encodedString,imageName);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                display.setImageBitmap(bitmap);
                Uri selectedImage = data.getData();
                //filename
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                File f = new File(picturePath);
                imageName = f.getName();
                //Toast.makeText(getApplicationContext(), imageName, Toast.LENGTH_SHORT).show();
                //encoded part of image

                ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
                byte [] imageBytes=byteArrayOutputStream.toByteArray();
                encodedString= Base64.encodeToString(imageBytes,Base64.DEFAULT);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    class ProcessSendPath extends AsyncTask<String, String, String> {
        private Activity activity;
        //Context context;
        ProgressDialog pd = new ProgressDialog(Facultyupload.this);
        HttpURLConnection conn;
        URL url = null;

        //public boolean x=false;
        public ProcessSendPath(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/facultyupload/facultyimage.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("fid", params[0]);
                builder.appendQueryParameter("image", params[1]);
                builder.appendQueryParameter("filename", params[2]);
                String query = builder.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            //Toast.makeText(Facultyupload.this, result, Toast.LENGTH_LONG).show();
            Intent i=new Intent(Facultyupload.this,Facultygrid.class);
            startActivity(i);
            finish();
            //res=result.toString();
            if (result.equalsIgnoreCase("1")) {
                Toast.makeText(getApplicationContext(), "1111", Toast.LENGTH_SHORT).show();

            } else if (result.equalsIgnoreCase("Login Failed")) {
                Toast.makeText(Facultyupload.this, "Login Failed", Toast.LENGTH_SHORT
                ).show();
            }
        }
    }
}

