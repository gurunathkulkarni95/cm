package com.example.pooja.cm;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Extracurriculardisplay extends AppCompatActivity {
    Config config=new Config();
    String usn;

    //this is the JSON Data URL
    //make sure you are using the correct ip else it will not work
    private static final String URL_PRODUCTS = "http://counsellingmanagementsystem.000webhostapp.com/extracurriculardisplay.php";

    //a list to store all the products
    List<Extracurricularproduct> productList;

    //the recyclerview
    RecyclerView recyclerView;

    TextView t1;

    private Handler mHandler;
    private Runnable mRunnable;
    private int mInterval = 300; // milliseconds
    private boolean initialState = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extracurriculardisplay);
        t1=(TextView)findViewById(R.id.text);

        //getting the recyclerview from xml
        recyclerView = findViewById(R.id.recylcerView);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                usn=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            usn=config.usn;
        }

       // Toast.makeText(getApplicationContext(),usn,Toast.LENGTH_SHORT).show();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        //initializing the productlist
        productList = new ArrayList<>();

        //this method will fetch and parse json
        //to display it in recyclerview
        loadProducts();
    }

    private void loadProducts() {

        /*
        * Creating a String Request
        * The request type is GET defined by first parameter
        * The URL is defined in the second parameter
        * Then we have a Response Listener and a Error Listener
        * In response listener we will get the JSON response as a String
        * */
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            JSONArray array = new JSONArray(response);

                            //traversing through all the object
                            for (int i = 0; i < array.length(); i++) {

                                //getting product object from json array
                                JSONObject product = array.getJSONObject(i);

                                //adding the product to product list
                                productList.add(new Extracurricularproduct(
                                        product.getString("usn"),
                                        product.getString("acyear"),
                                        product.getString("sem"),
                                        product.getString("fromdate"),
                                        product.getString("todate"),
                                        product.getString("eventname"),
                                        product.getString("host"),
                                        product.getString("eventlevel"),
                                        product.getString("award"),
                                        product.getString("financial"),
                                        product.getString("amount")
                                ));
                            }

                            //creating adapter object and setting it to recyclerview
                            Extracurriculardispadapter adapter = new Extracurriculardispadapter(Extracurriculardisplay.this, productList);
                            recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("usn", usn);
//Toast.makeText(getApplicationContext(),"sending",Toast.LENGTH_SHORT).show();
                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}