package com.example.pooja.cm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;

public class ActivityGrid extends AppCompatActivity {
    GridLayout maingrid;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);


        maingrid = (GridLayout) findViewById(R.id.grid);
        setSingleEvent(maingrid);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(ActivityGrid.this, Extracurricular.class);
                        startActivity(a);
                    } else if (choice == 1) {
                        Intent a = new Intent(ActivityGrid.this, Cocurricular.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(ActivityGrid.this, Scholarship.class);
                        startActivity(a);
                    }
                }
            });
        }
    }
}

