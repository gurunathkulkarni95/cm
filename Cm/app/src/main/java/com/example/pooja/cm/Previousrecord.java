package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class Previousrecord extends AppCompatActivity {
    EditText yop,board,inst,marks,yop1,board1,inst1,marks1,yop2,board2,inst2,marks2,sp1,sp2,sp3,c1,c2,c3;
Button b,clear;
    String us;
    Config config=new Config();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previousrecord);
        yop=(EditText)findViewById(R.id.usn);
        board=(EditText)findViewById(R.id.semester);
        inst=(EditText)findViewById(R.id.acyear);
        marks=(EditText)findViewById(R.id.date);
        yop1=(EditText)findViewById(R.id.purpose);
        board1=(EditText)findViewById(R.id.outcome);
        inst1=(EditText)findViewById(R.id.inst1);
        marks1=(EditText)findViewById(R.id.marks1);
        yop2=(EditText)findViewById(R.id.yop2);
        board2=(EditText)findViewById(R.id.board2);
        inst2=(EditText)findViewById(R.id.inst2);
        marks2=(EditText)findViewById(R.id.marks2);
        us=config.usn;
        b=(Button)findViewById(R.id.submit);
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yop.setText("");
                board.setText("");
                inst.setText("");
                marks.setText("");
                yop1.setText("");
                board1.setText("");
                inst1.setText("");
                marks1.setText("");
                yop2.setText("");
                board2.setText("");
                inst2.setText("");
                marks2.setText("");
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncReg reg = new AsyncReg(Previousrecord.this);
                reg.execute(us, yop.getText().toString(), board.getText().toString(),inst.getText().toString(),marks.getText().toString(),
                        yop1.getText().toString(), board1.getText().toString(),inst1.getText().toString(),marks1.getText().toString(),yop2.getText().toString(),
                        board2.getText().toString(),inst2.getText().toString(),marks2.getText().toString());
                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                startActivity(intent);
            }
        });


    }


class AsyncReg extends AsyncTask<String, String, String> {
    private Activity activity;
    String varConfirmation = "Welcome to Login page. Please Login !!";
    ProgressDialog pd = new ProgressDialog(Previousrecord.this);
    HttpURLConnection conn;
    URL url = null;

    public AsyncReg(Activity activity) {
        this.activity = activity;
    }

    protected void onPreExecute() {
        pd.setMessage("\t Please Wait !! Loading...");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            url = new URL("http://counsellingmanagementsystem.000webhostapp.com/studentpreviousrecord.php");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Exception";
        }
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            Uri.Builder builder;
            builder = new Uri.Builder().appendQueryParameter("us", params[0]);
            builder.appendQueryParameter("yop", params[1]);
            builder.appendQueryParameter("board", params[2]);
            builder.appendQueryParameter("inst", params[3]);
            builder.appendQueryParameter("marks", params[4]);
            builder.appendQueryParameter("yop1", params[5]);
            builder.appendQueryParameter("board1", params[6]);
            builder.appendQueryParameter("inst1", params[7]);
            builder.appendQueryParameter("marks1", params[8]);
            builder.appendQueryParameter("yop2", params[9]);
            builder.appendQueryParameter("board2", params[10]);
            builder.appendQueryParameter("inst2", params[11]);
            builder.appendQueryParameter("marks2", params[12]);
            String query = builder.build().getEncodedQuery();
            //String query1=;
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
            writer.write(query);
            // writer.write( query1 );
            writer.flush();
            writer.close();
            os.close();
            conn.connect();
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception : " + e.getMessage();
        }
        try {
            int response_code = conn.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                );
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                    result.append(line);
                return (result.toString());
            } else {
                return ("Unsuccessful");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ("Exception : " + e.getMessage());
        } finally {
            conn.disconnect();
        }
    }

    protected void onPostExecute(String result) {
        pd.dismiss();
        //Toast.makeText(Previousrecord.this, result, Toast.LENGTH_SHORT).show();
        if (result.equalsIgnoreCase("Successfully Registered!!")) {
            Toast.makeText(Previousrecord.this, "Registration Successful !!",
                    Toast.LENGTH_SHORT).show();
            activity.startActivity(new Intent(activity,Studentgrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
        } else if (result.equalsIgnoreCase("Try again please")) {
            Toast.makeText(Previousrecord.this, "Registration Failed. Try Again",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
}
