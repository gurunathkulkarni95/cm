package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Pstudentgrid extends AppCompatActivity {
    GridLayout maingrid;
    Bitmap myBitmap,bitmap1,b;
    ImageView image;
    String usn1,s1;
    TextView t1;

    Config config=new Config();
    String serverurl="http://counsellingmanagementsystem.000webhostapp.com/fetchimage.php";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pstudentgrid);


        maingrid = (GridLayout) findViewById(R.id.grid);
        image=(ImageView)findViewById(R.id.image);
        t1=(TextView)findViewById(R.id.usn);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                usn1=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            usn1=config.usn;
        }
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.b=bitmap1;
                Intent i=new Intent(Pstudentgrid.this,Studentimagedisplay.class);
                startActivity(i);
            }
        });

        t1.setText(usn1);
        // Toast.makeText(getApplicationContext(),usn1,Toast.LENGTH_SHORT).show();
        // getImage();
        //getImage();
        Downloadinamge downloadinamge=new Downloadinamge(Pstudentgrid.this);
        downloadinamge.execute(usn1);
        setSingleEvent(maingrid);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(Pstudentgrid.this, Studentdisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    } else if (choice == 1) {
                        Intent a = new Intent(Pstudentgrid.this, Previousdisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Pstudentgrid.this, Marksdisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    } else if (choice == 3) {
                        Intent a = new Intent(Pstudentgrid.this, Extracurriculardisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }  else if (choice == 4) {
                        Intent a = new Intent(Pstudentgrid.this, Cocurriculardisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 5) {
                        Intent a = new Intent(Pstudentgrid.this, Scholarshipdisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                }
            });
        }
    }




   /* void getImage()
    {

        ImageRequest imageRequest=new ImageRequest(serverurl,  new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
image.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_SHORT).show();
            }
        });
        MySingleton.getInstance(Studentgrid.this).addToRequestQueue(imageRequest);
    }*/




    class  Downloadinamge extends AsyncTask<String, Void, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Pstudentgrid.this);
        HttpURLConnection conn;
        URL url = null;

        public Downloadinamge(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/fetchimage.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                // return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);




                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    myBitmap = BitmapFactory.decodeStream(input);
                    s1=BitMapToString(myBitmap);





                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());


                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }




        }

        protected void onPostExecute(String result) {
            pd.dismiss();

            bitmap1=StringToBitMap(s1);
            image.setImageBitmap(bitmap1);

 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
        }


        public String BitMapToString(Bitmap bitmap){
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,50, baos);
            byte [] b=baos.toByteArray();
            String temp= Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }


        public Bitmap StringToBitMap(String encodedString){
            try {
                byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
                Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch(Exception e) {
                e.getMessage();
                return null;
            }
        }


    }
}










