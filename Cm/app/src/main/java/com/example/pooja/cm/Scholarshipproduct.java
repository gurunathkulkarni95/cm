package com.example.pooja.cm;


public class Scholarshipproduct {
    private String usn,name,organization,date,amount;


    public Scholarshipproduct(String usn, String name, String organization, String date,String amount) {
        this.usn= usn;
        this.name = name;
        this.organization=organization;
        this.date=date;
        this.amount=amount;
    }

    public String getUsn() {
        return usn;
    }

    public String getName() {
        return name;
    }

    public String getOrganization() {
        return organization;
    }

    public String getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }



}

