package com.example.pooja.cm;

/**
 * Created by gurunath on 12/04/18.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class Dnoticeadapter extends RecyclerView.Adapter<Dnoticeadapter.ProductViewHolder> {

    Config config=new Config();
    String fid;

    private Context mCtx;
    private List<Dnoticeproduct> productList;

    public Dnoticeadapter(Context mCtx, List<Dnoticeproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.dnotice, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        final Dnoticeproduct product = productList.get(position);


        //loading the image
        Glide.with(mCtx)
                .load(product.getImagepath())
                .into(holder.imageView);

        holder.textViewTitle.setText(product.getMessage());
        holder.textViewShortDesc.setText(product.getFid());
        fid=product.getFid();




        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path=product.getImagepath();
                Toast.makeText(mCtx,path,Toast.LENGTH_SHORT).show();
                new AsyncLogin().execute(path);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc, textViewPrice;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            imageView = itemView.findViewById(R.id.imageView);



        }

    }


    class AsyncLogin extends AsyncTask<String,String,String>
    {
        private Activity activity;
        //Context context;
        ProgressDialog pd = new ProgressDialog(mCtx);
        HttpURLConnection conn;
        URL url=null;
        //public boolean x=false;
        public AsyncLogin() {
            this.activity = activity;
        }
        protected void onPreExecute()
        {
            pd.setMessage( "\t Please Wait !! Loading..." );
            pd.setCancelable( false );
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try{
                url=new URL( "http://counsellingmanagementsystem.000webhostapp.com/deletenotice.php");
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception";
            }
            try{
                conn=(HttpURLConnection)url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(35000);
                conn.setRequestMethod("POST");
                conn.setDoInput( true );
                conn.setDoOutput( true );
                Uri.Builder builder;
                builder= new Uri.Builder().appendQueryParameter("path",params[0]);

                String query= builder.build().getEncodedQuery();
                OutputStream os=conn.getOutputStream();
                BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(os,"UTF8"));
                writer.write( query );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : "+e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader( new InputStreamReader( input )
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append( line );
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            }
            catch (IOException e){
                e.printStackTrace();
                return ("Exception : "+e.getMessage());
            }
            finally {
                conn.disconnect();
            }
        }
        protected void onPostExecute(String result)
        {
            pd.dismiss();
            Toast.makeText(mCtx,result, Toast.LENGTH_SHORT ).show();
            Intent i=new Intent(mCtx,Facultygrid.class);
            i.putExtra("Usna_NUmber",fid);
            i.putExtra("coming_from_Fstudent","100");
            mCtx.startActivity(i);
            }

        }
    }





