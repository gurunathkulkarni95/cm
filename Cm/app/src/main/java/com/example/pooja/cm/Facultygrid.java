package com.example.pooja.cm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.pooja.cm.R.id.grid;

public class Facultygrid extends AppCompatActivity {
    GridLayout maingrid;
    Bitmap myBitmap,bitmap1,b1;
    ImageView image;
    String fid,s1;
    TextView t1;
Config config=new Config();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultygrid);
        image=(ImageView)findViewById(R.id.image);


        fid=config.fid;
        t1=(TextView)findViewById(R.id.fid);
        t1.setText(fid);
        maingrid = (GridLayout) findViewById(grid);
        setSingleEvent(maingrid);
        Downloadinamge downloadinamge=new Downloadinamge(Facultygrid.this);
        downloadinamge.execute(fid);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ByteArrayOutputStream stream = new ByteArrayOutputStream();
                //bitmap1.compress(Bitmap.CompressFormat.PNG, 50, stream);
                //byte[] byteArray = stream.toByteArray();
                config.b=bitmap1;
                Intent i=new Intent(Facultygrid.this,Studentimagedisplay.class);

                //i.putExtra("image",byteArray);
                startActivity(i);
            }
        });
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(Facultygrid.this, Facultyform.class);
                        startActivity(a);
                    }
                     else if (choice == 1) {
                        Intent a = new Intent(Facultygrid.this, Fstudentdisplay.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Facultygrid.this, Noticeadd.class);
                        startActivity(a);
                    } else if (choice == 3) {
                        Intent a = new Intent(Facultygrid.this, Changepassword.class);
                        a.putExtra("Usna_NUmber",fid);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    } else if (choice == 4) {
                        Intent a = new Intent(Facultygrid.this, Facultyupload.class);
                        a.putExtra("Usna_NUmber",fid);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 5) {

                        Intent a = new Intent(Facultygrid.this, DeleteNotice.class);
                        a.putExtra("Usna_NUmber",fid);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);

                    }
                    else if (choice == 6) {
                        logoutOperationa();
                    }
                }
            });
        }
    }




    class  Downloadinamge extends AsyncTask<String, Void, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Facultygrid.this);
        HttpURLConnection conn;
        URL url = null;

        public Downloadinamge(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/facimagefetch.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                // return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("fid", params[0]);




                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    myBitmap = BitmapFactory.decodeStream(input);
                    s1=BitMapToString(myBitmap);





                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());


                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }




        }

        protected void onPostExecute(String result) {
            pd.dismiss();

            bitmap1=StringToBitMap(s1);

            image.setImageBitmap(bitmap1);

 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
        }


        public String BitMapToString(Bitmap bitmap){
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,50, baos);
            byte [] b=baos.toByteArray();
            String temp= Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }


        public Bitmap StringToBitMap(String encodedString){
            try {
                byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
                Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch(Exception e) {
                e.getMessage();
                return null;
            }
        }


    }






    private void logoutOperationa() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Facultygrid.this);
        builder1.setMessage("Do you want to logout now ?");
        builder1.setCancelable(true);
        builder1.setTitle("Log Out");

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent intent = new Intent(Facultygrid.this, MainActivity
                                .class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                });

        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog logOutDialog = builder1.create();
        logOutDialog.show();

    }

    @Override
    public void onBackPressed() {
        logoutOperationa();
    }
}

