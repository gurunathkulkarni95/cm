package com.example.pooja.cm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Belal on 10/18/2017.
 */

public class Facultydispadapter extends RecyclerView.Adapter<Facultydispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<FacultyProduct> productList;
    FacultyDisplayModel model;

    public Facultydispadapter(Context mCtx, FacultyDisplayModel model) {
        this.mCtx = mCtx;
        this.model = model;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.facultydisp_list, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //  Product product = productList.get(position);

        //loading the image
        /**Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(model.getFacultyname());
        holder.t2.setText(model.getFacultyid());
        holder.t3.setText(model.getDesignation());
        holder.t4.setText(model.getEmailid());
        holder.t5.setText(model.getContact());
        holder.t6.setText(model.getSubject());
        holder.t7.setText(model.getSubjectcode());
        holder.t8.setText(model.getSem());

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8;
        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3= itemView.findViewById(R.id.t3);
            t4= itemView.findViewById(R.id.t4);
            t5= itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);
            t8= itemView.findViewById(R.id.t8);


        }
    }
}

