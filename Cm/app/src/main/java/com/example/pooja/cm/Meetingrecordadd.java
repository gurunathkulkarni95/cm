package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Meetingrecordadd extends AppCompatActivity {
    String fid;
    EditText usn,semester,acyear,date,purpose,outcome;
    Button submit,clear;
    Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meetingrecordadd);
        usn=(EditText)findViewById(R.id.usn);
        semester=(EditText)findViewById(R.id.semester);
        acyear=(EditText)findViewById(R.id.acyear);
        date=(EditText)findViewById(R.id.date);
        purpose=(EditText)findViewById(R.id.purpose);
        outcome=(EditText)findViewById(R.id.outcome);
        submit=(Button)findViewById(R.id.submit);
        clear=(Button)findViewById(R.id.clear);

        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                fid=getIntent().getStringExtra("Usna_NUmber");
                //Toast.makeText(getApplicationContext(),fid,Toast.LENGTH_SHORT).show();
            }
        }

        else{
            fid=config.fid;
        }
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usn.setText("");
                semester.setText("");
                acyear.setText("");
                date.setText("");
                purpose.setText("");
                outcome.setText("");
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncReg reg = new AsyncReg(Meetingrecordadd.this);
                reg.execute(usn.getText().toString(),fid, semester.getText().toString(),acyear.getText().toString(),date.getText().toString(),
                        purpose.getText().toString(), outcome.getText().toString());
                //Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
               // startActivity(intent);
            }
        });

    }



    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Meetingrecordadd.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/meetingrecordadd.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("fid", params[1]);
                builder.appendQueryParameter("semester", params[2]);
                builder.appendQueryParameter("acyear", params[3]);
                builder.appendQueryParameter("date", params[4]);
                builder.appendQueryParameter("purpose", params[5]);
                builder.appendQueryParameter("outcome", params[6]);
                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Meetingrecordadd.this, result, Toast.LENGTH_SHORT).show();
           // Intent intent=new Intent(getApplicationContext(),Counsellorgrid.class);
            Intent i=new Intent(getApplicationContext(),Counsellorgrid.class);
            i.putExtra("Usna_NUmber",fid);
            i.putExtra("coming_from_Fstudent","100");
            startActivity(i);
            finish();

            if (result.equalsIgnoreCase("Successfully Registered!!")) {
                Toast.makeText(Meetingrecordadd.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity,Studentgrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Meetingrecordadd.this, "Registration Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }




}
