package com.example.pooja.cm;

public class Noticeproduct {
    private String fname;
    private String message;
    private String image;
    private String image1;


    public Noticeproduct(String fname, String message,String image,String image1) {
        this.fname = fname;
        this.message = message;
        this.image = image;
        this.image1=image1;

    }

    public String getFname() {
        return fname;
    }

    public String getMessage() {
        return message;
    }

    public String getImage() {
        return image;
    }
    public String getImage1() {
        return image1;
    }
}


