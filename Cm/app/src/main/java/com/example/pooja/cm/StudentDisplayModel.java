package com.example.pooja.cm;


public class StudentDisplayModel {

    /**
     * firstname : pooja
     * midname : ganesh
     * lastname : nandnikar
     * usn : 2BA14IS035
     * cetranking : 21606
     * course : Information Science and Engineering
     * yearofadmission : 2014
     * contactno : 8553438054
     * dateofbirth : 1996-06-26
     * mode : CET
     * email : poohnandnikar@gmail.com
     * drivinglicense : bnbbb
     * passport : eeee
     * aadharno : eed
     * bloodgroup : o+ve
     * fathername : ganesh
     * fatheroccupation : retd govt officer
     * fathercontact : 8884506073
     * mothername : veena
     * motheroccupation : home maker
     * mothercontact : 8553438054
     * csn : 20179394
     * residential : bdcjdhjdd
     */

    private String firstname;
    private String midname;
    private String lastname;
    private String usn;
    private String cetranking;
    private String course;
    private String yearofadmission;
    private String contactno;
    private String dateofbirth;
    private String mode;
    private String email;
    private String drivinglicense;
    private String passport;
    private String aadharno;
    private String bloodgroup;
    private String fathername;
    private String fatheroccupation;
    private String fathercontact;
    private String mothername;
    private String motheroccupation;
    private String mothercontact;
    private String csn;
    private String residential;
   // private String imagepath;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMidname() {
        return midname;
    }

    public void setMidname(String midname) {
        this.midname = midname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getCetranking() {
        return cetranking;
    }

    public void setCetranking(String cetranking) {
        this.cetranking = cetranking;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getYearofadmission() {
        return yearofadmission;
    }

    public void setYearofadmission(String yearofadmission) {
        this.yearofadmission = yearofadmission;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDrivinglicense() {
        return drivinglicense;
    }

    public void setDrivinglicense(String drivinglicense) {
        this.drivinglicense = drivinglicense;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getAadharno() {
        return aadharno;
    }

    public void setAadharno(String aadharno) {
        this.aadharno = aadharno;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getFatheroccupation() {
        return fatheroccupation;
    }

    public void setFatheroccupation(String fatheroccupation) {
        this.fatheroccupation = fatheroccupation;
    }

    public String getFathercontact() {
        return fathercontact;
    }

    public void setFathercontact(String fathercontact) {
        this.fathercontact = fathercontact;
    }

    public String getMothername() {
        return mothername;
    }

    public void setMothername(String mothername) {
        this.mothername = mothername;
    }

    public String getMotheroccupation() {
        return motheroccupation;
    }

    public void setMotheroccupation(String motheroccupation) {
        this.motheroccupation = motheroccupation;
    }

    public String getMothercontact() {
        return mothercontact;
    }

    public void setMothercontact(String mothercontact) {
        this.mothercontact = mothercontact;
    }

    public String getCsn() {
        return csn;
    }

    public void setCsn(String csn) {
        this.csn = csn;
    }

    public String getResidential() {
        return residential;
    }

    public void setResidential(String residential) {
        this.residential = residential;
    }

   /** public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath= imagepath;
    }**/


}
