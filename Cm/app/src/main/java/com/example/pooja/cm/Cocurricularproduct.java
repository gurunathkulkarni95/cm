package com.example.pooja.cm;


public class Cocurricularproduct {
    private String usn,acyear,sem,fromdate,todate,eventname,host,eventlevel,award,financial,amount;


    public Cocurricularproduct(String usn, String acyear, String sem, String fromdate,String todate, String eventname, String host, String eventlevel,String award, String financial, String amount) {
        this.usn= usn;
        this.acyear = acyear;
        this.sem=sem;
        this.fromdate= fromdate;
        this.todate=todate;
        this.eventname=eventname;
        this.host=host;
        this.eventlevel= eventlevel;
        this.award=award;
        this.financial=financial;
        this.amount=amount;
    }

    public String getUsn() {
        return usn;
    }

    public String getAcyear() {
        return acyear;
    }

    public String getSem() {
        return sem;
    }

    public String getFromdate() {
        return fromdate;
    }
    public String getTodate() {
        return todate;
    }

    public String getEventname() {
        return eventname;
    }

    public String getHost() {
        return host;
    }

    public String getEventlevel() {
        return eventlevel;
    }
    public String getAward() {
        return award;
    }

    public String getFinancial() {
        return financial;
    }

    public String getAmount() {
        return amount;
    }



}

