package com.example.pooja.cm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Marksentryinst extends AppCompatActivity {

    String usn;
    Config config;
    Button n1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marksentryinst);


        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                usn=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            usn=config.usn;
        }
        n1=(Button)findViewById(R.id.id1);
        n1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Marksentryinst.this, Cieform.class);
                a.putExtra("Usna_NUmber",usn);
                a.putExtra("coming_from_Fstudent","100");
                startActivity(a);

            }
        });
    }
}
