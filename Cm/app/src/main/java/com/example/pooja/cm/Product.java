package com.example.pooja.cm;


public class Product {
    private String firstname,midname,lastname,usn,cetranking,course,yearofadmission,contacno,dateofbirth,mode,email,drivinglicense,passport,aadharno,bloodgroup,fathername,fatheroccupation,fathercontact,mothername,motheroccupation,mothercontact,csn,residential;


    public Product(String firstname, String midname, String lastname, String usn, String cetranking, String course,String yearofadmission,String contactno,String dateofbirth,
                   String mode,String email,String drivinglicense,String passport,String aadharno,String bloodgroup,String fathername,String fatheroccupation,String fathercontact,String mothername,String motheroccupation,String mothercontact,
                   String csn,String residential) {
        this.firstname= firstname;
        this.midname = midname;
        this.lastname = lastname;
        this.usn= usn;
        this.cetranking= cetranking;
        this.course= course;
        this.yearofadmission= yearofadmission;
        this.contacno = contactno;
        this.dateofbirth= dateofbirth;
        this.mode = mode;
        this.email= email;
        this.drivinglicense= drivinglicense;
        this.passport= passport;
        this.aadharno = aadharno;
        this.bloodgroup = bloodgroup;
        this.fathername = fathername;
        this.fatheroccupation= fatheroccupation;
        this.fathercontact= fathercontact;
        this.mothername = mothername;
        this.motheroccupation= motheroccupation;
        this.mothercontact= mothercontact;
        this.csn = csn;
        this.residential= residential;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMidname() {
        return midname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsn() {
        return usn;
    }

    public String getCetranking() {
        return cetranking;
    }

    public String getCourse() {
        return course;
    }

    public String getYearofadmission() {
        return yearofadmission;
    }

    public String getContacno() {
        return contacno;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public String getMode() {
        return mode;
    }

    public String getEmail() {
        return email;
    }

    public String getDrivinglicense() {
        return drivinglicense;
    }

    public String getPassport() {
        return passport;
    }

    public String getAadharno() {
        return aadharno;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }
    public String getFathername() {
        return fathername;
    }

    public String getFatheroccupation() {
        return fatheroccupation;
    }

    public String getFathercontact() {
        return fathercontact;
    }

    public String getMothername() {
        return mothername;
    }

    public String getMotheroccupation() {
        return motheroccupation;
    }

    public String getMothercontact() {
        return mothercontact;
    }

    public String getCsn() {
        return csn;
    }
    public String getResidential(){
        return residential;
    }
}

