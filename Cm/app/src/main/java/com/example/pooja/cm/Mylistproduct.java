package com.example.pooja.cm;


public class Mylistproduct {
    private String firstname,midname,lastname,usn;


    public Mylistproduct(String firstname, String midname, String lastname, String usn) {
        this.firstname= firstname;
        this.midname = midname;
        this.lastname = lastname;
        this.usn= usn;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMidname() {
        return midname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsn() {
        return usn;
    }

}

