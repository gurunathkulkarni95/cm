package com.example.pooja.cm;


public class FacultyDisplayModel {

    /**
     * firstname : pooja
     * midname : ganesh
     * lastname : nandnikar
     * usn : 2BA14IS035
     * cetranking : 21606
     * course : Information Science and Engineering
     * yearofadmission : 2014
     * contactno : 8553438054
     * dateofbirth : 1996-06-26
     * mode : CET
     * email : poohnandnikar@gmail.com
     * drivinglicense : bnbbb
     * passport : eeee
     * aadharno : eed
     * bloodgroup : o+ve
     * fathername : ganesh
     * fatheroccupation : retd govt officer
     * fathercontact : 8884506073
     * mothername : veena
     * motheroccupation : home maker
     * mothercontact : 8553438054
     * csn : 20179394
     * residential : bdcjdhjdd
     */

    private String facultyname;
    private String facultyid;
    private String designation;
    private String emailid;
    private String contact;
    private String subject;
    private String subjectcode;
    private String sem;


    public String getFacultyname() {
        return facultyname;
    }

    public void setFacultyname(String Facultyname) {
        this.facultyname = facultyname;
    }

    public String getFacultyid() {
        return facultyid;
    }

    public void setFacultyid(String Facultyid) {
        this.facultyid = facultyid;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation= designation;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubjectcode() {
        return subjectcode;
    }

    public void setSubjectcode(String subjectcode) {
        this.subjectcode = subjectcode;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

}
