package com.example.pooja.cm;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    GridLayout maingrid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        maingrid=(GridLayout)findViewById(R.id.grid);
        setSingleEvent(maingrid);
    }

    private void setSingleEvent(GridLayout maingrid) {
        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(MainActivity.this, Login.class);
                        startActivity(a);
                    }
                    else if(choice==1)
                    {
                        Intent a = new Intent(MainActivity.this, facultylogin.class);
                        startActivity(a);
                    }
                    else if(choice==2)
                    {
                        Intent a = new Intent(MainActivity.this, Counsellorlogin.class);
                        startActivity(a);
                    }
                    else if(choice==3)
                    {
                        Intent a = new Intent(MainActivity.this, Parent_login.class);
                        startActivity(a);
                    }

                    else if(choice==4)
                    {
                        Intent a = new Intent(MainActivity.this, Adminlogin.class);
                        startActivity(a);
                    }
                    else if(choice==5)
                    {
                        Intent a = new Intent(MainActivity.this, Noticedisplay.class);
                        startActivity(a);
                    }
                    else if(choice==6)
                    {
                        Intent a = new Intent(MainActivity.this, Appinfo.class);
                        startActivity(a);
                    }
                }
            });






        }
    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
