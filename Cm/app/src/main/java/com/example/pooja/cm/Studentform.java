package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Studentform extends AppCompatActivity  {

    public String mode1,mode2;
    EditText firstname,middlename,lastname,usn,csn,cetrank,year,mobile_no,aadharno,email,dateofbirth,drivinglicense,fathername,mothername;
    EditText fatheroccupation,motheroccuption,fatherscontact,motherscontact,postaladdress,blood,pass;
    Spinner course,mode;
    Button b1,clear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentform);
        firstname=(EditText)findViewById(R.id.first);
        middlename=(EditText)findViewById(R.id.mid);
        lastname=(EditText)findViewById(R.id.last);
        usn=(EditText)findViewById(R.id.usn);
        cetrank=(EditText)findViewById(R.id.cetrank);
        course=(Spinner)findViewById(R.id.course);
        year=(EditText)findViewById(R.id.yearof);
        mobile_no=(EditText)findViewById(R.id.contact);
        dateofbirth=(EditText)findViewById(R.id.dob);
        mode=(Spinner) findViewById(R.id.mode);
        email=(EditText)findViewById(R.id.email);
        drivinglicense=(EditText)findViewById(R.id.dl);
        pass=(EditText)findViewById(R.id.pass);
        aadharno=(EditText)findViewById(R.id.aadhar);
        blood=(EditText)findViewById(R.id.blood);
        fathername=(EditText)findViewById(R.id.fathername);
        fatheroccupation=(EditText)findViewById(R.id.fatheroccu);
        fatherscontact=(EditText)findViewById(R.id.fathercontact);
        mothername=(EditText)findViewById(R.id.mothername);
        motheroccuption=(EditText)findViewById(R.id.moccu);
        motherscontact=(EditText)findViewById(R.id.mcontact);
        csn=(EditText)findViewById(R.id.csn);
        postaladdress=(EditText)findViewById(R.id.res);
        b1=(Button)findViewById(R.id.submit);
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstname.setText("");
                middlename.setText("");
                lastname.setText("");
                usn.setText("");
                csn.setText("");
                cetrank.setText("");
                year.setText("");
                mobile_no.setText("");
                aadharno.setText("");
                email.setText("");
                dateofbirth.setText("");
                drivinglicense.setText("");
                fathername.setText("");
                mothername.setText("");
                fatheroccupation.setText("");
                motheroccuption.setText("");
                fatherscontact.setText("");
                motherscontact.setText("");
                postaladdress.setText("");
                blood.setText("");
                pass.setText("");

            }
        });
        course.setPrompt("course?");
        mode.setPrompt("mode of admission");
        ArrayAdapter adapter1=ArrayAdapter.createFromResource(this,R.array.mode,android.R.layout.simple_spinner_item);
        mode.setAdapter(adapter1);
        mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode2="CET";
                        break;
                    case 1:mode2="COMEDK";
                        break;
                    case 2:mode2="Management";
                        break;
                    case 3:mode2="DCET";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter adapter2=ArrayAdapter.createFromResource(this,R.array.course,android.R.layout.simple_spinner_item);
        course.setAdapter(adapter2);
        course.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode1="Information Science and Engineering";
                        break;
                    case 1:mode1="Computer Science and Engineering";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });







        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Studentform.this,mode1,Toast.LENGTH_SHORT).show();
                Toast.makeText(Studentform.this,mode2,Toast.LENGTH_SHORT).show();

                AsyncReg reg = new AsyncReg(Studentform.this);

                reg.execute(firstname.getText().toString(), middlename.getText().toString(), lastname.getText().toString(),usn.getText().toString(),cetrank.getText().toString(),mode1,year.getText().toString(),mobile_no.getText().toString(),dateofbirth.getText().toString(),
                        mode2, email.getText().toString(),drivinglicense.getText().toString(),pass.getText().toString(),aadharno.getText().toString(),blood.getText().toString(),fathername.getText().toString(),fatheroccupation.getText().toString(),
                        fatherscontact.getText().toString() ,  mothername.getText().toString(),motheroccuption.getText().toString(),motherscontact.getText().toString(),csn.getText().toString(),postaladdress.getText().toString());
                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                startActivity(intent);
            }
        });


    }
    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Studentform.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/studentform.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("firstname", params[0]);
                builder.appendQueryParameter("middlename", params[1]);
                builder.appendQueryParameter("lastname", params[2]);
                builder.appendQueryParameter("usn", params[3]);
                builder.appendQueryParameter("cetrank", params[4]);
                builder.appendQueryParameter("mode1", params[5]);
                builder.appendQueryParameter("year", params[6]);
                builder.appendQueryParameter("mobile_no", params[7]);
                builder.appendQueryParameter("dateofbirth", params[8]);
                builder.appendQueryParameter("mode2", params[9]);
                builder.appendQueryParameter("email", params[10]);
                builder.appendQueryParameter("drivinglicense", params[11]);
                builder.appendQueryParameter("pass", params[12]);
                builder.appendQueryParameter("aadharno", params[13]);
                builder.appendQueryParameter("blood", params[14]);
                builder.appendQueryParameter("fathername", params[15]);
                builder.appendQueryParameter("fatheroccupation", params[16]);
                builder.appendQueryParameter("fatherscontact", params[17]);
                builder.appendQueryParameter("mothername", params[18]);
                builder.appendQueryParameter("motheroccupation", params[19]);
                builder.appendQueryParameter("motherscontact", params[20]);
                builder.appendQueryParameter("csn", params[21]);
                builder.appendQueryParameter("postaladdress", params[22]);
                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Studentform.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully Registered!!")) {
                Toast.makeText(Studentform.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Studentgrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Studentform.this, "Registration Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
