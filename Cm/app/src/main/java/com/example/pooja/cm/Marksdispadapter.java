package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class Marksdispadapter extends RecyclerView.Adapter<Marksdispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<Marksproduct> productList;

    public Marksdispadapter(Context mCtx, List<Marksproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.marks_list, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Marksproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(product.getUsn());
        holder.t2.setText(product.getSubjectcode());
        holder.t3.setText(product.getSem());
        holder.t4.setText(product.getYear());
        holder.t5.setText(product.getCie1());
        holder.t6.setText(product.getCie2());
        holder.t7.setText(product.getCie3());
        holder.t8.setText(product.getCompensatory());
        holder.t9.setText(product.getAssignment());
        holder.t10.setText(product.getGrade());
        holder.t11.setText(product.getCredit());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3 = itemView.findViewById(R.id.t3);
            t4 = itemView.findViewById(R.id.t4);
            t5 = itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);
            t8 = itemView.findViewById(R.id.t8);
            t9 = itemView.findViewById(R.id.t9);
            t10 = itemView.findViewById(R.id.t10);
            t11= itemView.findViewById(R.id.t11);
            //imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
