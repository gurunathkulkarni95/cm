package com.example.pooja.cm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Belal on 10/18/2017.
 */

public class Previousdispadapter extends RecyclerView.Adapter<Previousdispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<Previousproduct> productList;
    Previousdisplaymodel model;

    public Previousdispadapter(Context mCtx, Previousdisplaymodel model) {
        this.mCtx = mCtx;
        this.model = model;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.previouslist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //  Product product = productList.get(position);

        //loading the image
        /**Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(model.getUsn());
        holder.t2.setText(model.getYop1());
        holder.t3.setText(model.getBoard1());
        holder.t4.setText(model.getInst1());
        holder.t5.setText(model.getMarks1());
        holder.t6.setText(model.getYop2());
        holder.t7.setText(model.getBoard2());
        holder.t8.setText(model.getInst2());
        holder.t9.setText(model.getMarks2());
        holder.t10.setText(model.getYop3());
        holder.t11.setText(model.getBoard3());
        holder.t12.setText(model.getInst3());
        holder.t13.setText(model.getMarks3());


    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13;
        TextView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3= itemView.findViewById(R.id.t3);
            t4= itemView.findViewById(R.id.t4);
            t5= itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);
            t8= itemView.findViewById(R.id.t8);
            t9= itemView.findViewById(R.id.t9);
            t10= itemView.findViewById(R.id.t10);
            t11= itemView.findViewById(R.id.t11);
            t12= itemView.findViewById(R.id.t12);
            t13= itemView.findViewById(R.id.t13);


        }
    }
}

