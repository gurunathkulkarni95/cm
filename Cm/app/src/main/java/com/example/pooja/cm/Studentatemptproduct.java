
package com.example.pooja.cm;

import static com.example.pooja.cm.Config.usn;

public class Studentatemptproduct {
    private String subjectname,subjectcode,attempts;


    public Studentatemptproduct(String subjectname, String subjectcode, String attempts) {
        this.subjectname= subjectname;
        this.subjectcode = subjectcode;
        this.attempts = attempts;
    }

    public String getSubjectname() {
        return subjectname;
    }

    public String getSubjectcode() {
        return subjectcode;
    }

    public String getAttempts() {
        return attempts;
    }

    public String getUsn() {
        return usn;
    }

}