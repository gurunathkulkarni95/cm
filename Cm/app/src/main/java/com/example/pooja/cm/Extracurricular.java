package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Extracurricular extends AppCompatActivity{
String usn;
    public String mode,mode1,mode2,mode3,mode4;
    EditText academic,tdate,fdate,eventname,amount,other;
    Spinner sem,eventlevel,host,award,financial;
    Config config=new Config();

    Button b1,clear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extracurricular);
        academic=(EditText)findViewById(R.id.co1);
        fdate=(EditText)findViewById(R.id.co3);
        tdate=(EditText)findViewById(R.id.co4);
        eventname=(EditText)findViewById(R.id.co5);
        amount=(EditText)findViewById(R.id.co10);
        sem=(Spinner) findViewById(R.id.co2);
        host=(Spinner) findViewById(R.id.co6);
        eventlevel=(Spinner)findViewById(R.id.co7);
        award=(Spinner)findViewById(R.id.co8);
        financial=(Spinner)findViewById(R.id.co9);
        b1=(Button)findViewById(R.id.submit);
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                academic.setText("");
                fdate.setText("");
                tdate.setText("");
                eventname.setText("");
                amount.setText("");

            }
        });
        sem.setPrompt("Semester");
        host.setPrompt("Organizer");
        eventlevel.setPrompt("event level");
        award.setPrompt("which prize?");
        financial.setPrompt("Financial assistance by college");
        ArrayAdapter adapter1=ArrayAdapter.createFromResource(this,R.array.des,android.R.layout.simple_spinner_item);
        sem.setAdapter(adapter1);
        sem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode="1";
                        break;
                    case 1:mode="2";
                        break;
                    case 2:mode="3";
                        break;
                    case 3:mode="4";
                        break;

                    case 4:mode="5";
                        break;
                    case 5:mode="6";
                        break;
                    case 6:mode="7";
                        break;
                    case 7:mode="8";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter adapter2=ArrayAdapter.createFromResource(this,R.array.co6,android.R.layout.simple_spinner_item);
        host.setAdapter(adapter2);
        host.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode1="BEC";
                        break;
                    case 1:mode1="OTHER COLLEGES";

                        //other.setVisibility(View.VISIBLE);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter adapter3=ArrayAdapter.createFromResource(this,R.array.co7,android.R.layout.simple_spinner_item);
        eventlevel.setAdapter(adapter3);
        eventlevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode2="college level";
                        break;
                    case 1:mode2="department level";
                        break;

                    case 2:mode2="national level";
                        break;

                    case 3:mode2="zonal level";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter adapter4=ArrayAdapter.createFromResource(this,R.array.co8,android.R.layout.simple_spinner_item);
        award.setAdapter(adapter4);
        award.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode3="1";
                        break;
                    case 1:mode3="2";
                        break;

                    case 2:mode3="3";
                        break;

                    case 3:mode3="no award";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter adapter5=ArrayAdapter.createFromResource(this,R.array.co9,android.R.layout.simple_spinner_item);
        financial.setAdapter(adapter5);
        financial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode4="yes";
                        break;
                    case 1:mode4="no";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });







        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncReg reg = new AsyncReg(Extracurricular.this);
                usn=config.usn;

                reg.execute(usn,academic.getText().toString(), mode,fdate.getText().toString(), tdate.getText().toString(),eventname.getText().toString(),mode1,mode2,mode3,mode4,amount.getText().toString());
                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                startActivity(intent);
            }
        });


    }
    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Extracurricular.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/extracurricular.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("academic", params[1]);
                builder.appendQueryParameter("mode", params[2]);
                builder.appendQueryParameter("fdate", params[3]);
                builder.appendQueryParameter("tdate", params[4]);
                builder.appendQueryParameter("eventname", params[5]);
                builder.appendQueryParameter("mode1", params[6]);
                builder.appendQueryParameter("mode2", params[7]);
                builder.appendQueryParameter("mode3", params[8]);
                builder.appendQueryParameter("mode4", params[9]);
                builder.appendQueryParameter("amount", params[10]);


                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Extracurricular.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully form filled!!")) {
                Toast.makeText(Extracurricular.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Facultygrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Extracurricular.this, "form fill up Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

