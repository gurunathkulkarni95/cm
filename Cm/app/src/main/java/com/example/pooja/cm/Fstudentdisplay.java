package com.example.pooja.cm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Fstudentdisplay extends AppCompatActivity {
    EditText e;
    Button b;
    String usn;
Config config=new Config();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fstudentdisplay);
        e=(EditText)findViewById(R.id.us);
        b=(Button)findViewById(R.id.su);
        usn=e.getText().toString();
        config.usn=usn;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Fstudentdisplay.this,Fstudentgrid.class);
                i.putExtra("Usna_NUmber",e.getText().toString());
                i.putExtra("coming_from_Fstudent","100");
                startActivity(i);
                finish();
            }
        });
    }
}
