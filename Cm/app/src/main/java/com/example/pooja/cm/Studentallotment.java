package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;



public class Studentallotment extends AppCompatActivity {
    EditText a,b;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentallotment);
        a=(EditText)findViewById(R.id.a);
        b=(EditText)findViewById(R.id.b);
        bt=(Button)findViewById(R.id.bt);
        String username,pass1,email;

            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AsyncReg reg = new AsyncReg(Studentallotment.this);

                    reg.execute(a.getText().toString(), b.getText().toString());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            });
        }

        class AsyncReg extends AsyncTask<String, String, String> {
            private Activity activity;
            String varConfirmation = "Welcome to Login page. Please Login !!";
            ProgressDialog pd = new ProgressDialog(Studentallotment.this);
            HttpURLConnection conn;
            URL url = null;

            public AsyncReg(Activity activity) {
                this.activity = activity;
            }

            protected void onPreExecute() {
                pd.setMessage("\t Please Wait !! Loading...");
                pd.setCancelable(false);
                pd.show();
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    url = new URL("http://counsellingmanagementsystem.000webhostapp.com/studallotment.php");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return "Exception";
                }
                try {
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    Uri.Builder builder;
                    builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                    builder.appendQueryParameter("fid", params[1]);
                    //builder.appendQueryParameter("password", params[2]);
                    String query = builder.build().getEncodedQuery();
                    //String query1=;
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                    writer.write(query);
                    // writer.write( query1 );
                    writer.flush();
                    writer.close();
                    os.close();
                    conn.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Exception : " + e.getMessage();
                }
                try {
                    int response_code = conn.getResponseCode();
                    if (response_code == HttpURLConnection.HTTP_OK) {
                        InputStream input = conn.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                        );
                        StringBuilder result = new StringBuilder();
                        String line;
                        while ((line = reader.readLine()) != null)
                            result.append(line);
                        return (result.toString());
                    } else {
                        return ("Unsuccessful");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return ("Exception : " + e.getMessage());
                } finally {
                    conn.disconnect();
                }
            }

            protected void onPostExecute(String result) {
                pd.dismiss();
                Toast.makeText(Studentallotment.this, result, Toast.LENGTH_SHORT).show();
                if (result.equalsIgnoreCase("Successfully Registered!!")) {
                    Toast.makeText(Studentallotment.this, "Registration Successful !!",
                            Toast.LENGTH_SHORT).show();
                    activity.startActivity(new Intent(activity, Studentallotment.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
                } else if (result.equalsIgnoreCase("Try again please")) {
                    Toast.makeText(Studentallotment.this, "Registration Failed. Try Again",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


