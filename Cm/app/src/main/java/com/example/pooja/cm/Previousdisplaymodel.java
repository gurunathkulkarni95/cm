package com.example.pooja.cm;


public class Previousdisplaymodel {

    /**
     * firstname : pooja
     * midname : ganesh
     * lastname : nandnikar
     * usn : 2BA14IS035
     * cetranking : 21606
     * course : Information Science and Engineering
     * yearofadmission : 2014
     * contactno : 8553438054
     * dateofbirth : 1996-06-26
     * mode : CET
     * email : poohnandnikar@gmail.com
     * drivinglicense : bnbbb
     * passport : eeee
     * aadharno : eed
     * bloodgroup : o+ve
     * fathername : ganesh
     * fatheroccupation : retd govt officer
     * fathercontact : 8884506073
     * mothername : veena
     * motheroccupation : home maker
     * mothercontact : 8553438054
     * csn : 20179394
     * residential : bdcjdhjdd
     */

    private String usn;
    private String yop1;
    private String board1;
    private String inst1;
    private String marks1;
    private String yop2;
    private String board2;
    private String inst2;
    private String marks2;
    private String yop3;
    private String board3;
    private String inst3;
    private String marks3;


    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getYop1() {
        return yop1;
    }

    public void setYop1(String yop1) {
        this.yop1 = yop1;
    }

    public String getBoard1() {
        return board1;
    }

    public void setBoard1(String board1) {
        this.board1 = board1;
    }

    public String getInst1() {
        return inst1;
    }

    public void setInst1(String inst1) {
        this.inst1 = inst1;
    }

    public String getMarks1() {
        return marks1;
    }

    public void setMarks1(String marks1) {
        this.marks1 = marks1;
    }

    public String getYop2() {
        return yop2;
    }

    public void setYop2(String yop2) {
        this.yop2= yop2;
    }

    public String getBoard2() {
        return board2;
    }

    public void setBoard2(String board2) {
        this.board2 = board2;
    }

    public String getInst2() {
        return inst2;
    }

    public void setInst2(String inst2) {
        this.inst2= inst2;
    }

    public String getMarks2() {
        return marks2;
    }

    public void setMarks2(String marks2) {
        this.marks2= marks2;
    }


    public String getYop3() {
        return yop3;
    }

    public void setYop3(String yop3) {
        this.yop3 = yop3;
    }

    public String getBoard3() {
        return board3;
    }

    public void setBoard3(String board3) {
        this.board3 = board3;
    }

    public String getInst3() {
        return inst3;
    }

    public void setInst3(String inst3) {
        this.inst3= inst3;
    }

    public String getMarks3() {
        return marks3;
    }

    public void setMarks3(String marks3) {
        this.marks3= marks3;
    }


}
