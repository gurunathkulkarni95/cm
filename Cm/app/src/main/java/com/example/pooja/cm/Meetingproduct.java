package com.example.pooja.cm;


public class Meetingproduct {
    private String usn,fid,semester,acyear,date,purpose,outcome;


    public Meetingproduct(String usn, String fid, String semester, String acyear,String date, String purpose, String outcome) {
        this.usn= usn;
        this.fid = fid;
        this.semester=semester;
        this.acyear= acyear;
        this.date= date;
        this.purpose=purpose;
        this.outcome=outcome;
    }

    public String getUsn() {
        return usn;
    }

    public String getFid() {return fid;}

    public String getSemester() {
        return semester;
    }

    public String getAcyear() {
        return acyear;
    }
    public String getDate() {
        return date;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getOutcome() {return outcome; }

}

