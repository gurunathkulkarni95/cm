package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DeleteNotice extends AppCompatActivity {
    Button deleteall,delete;
    String fid;
    Config config;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_notice);
        deleteall=(Button)findViewById(R.id.deleteall);
        delete=(Button)findViewById(R.id.delete);
        String s=getIntent().getStringExtra("coming_from_Fstudent");
        if(s!=null){
            if(s.equalsIgnoreCase("100")){
                fid=getIntent().getStringExtra("Usna_NUmber");
            }
        }

        else{
            fid=config.fid;
        }


        deleteall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AsyncLogin login=new AsyncLogin(DeleteNotice.this);
                login.execute(fid);



            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),Deletedisplay.class);
                intent.putExtra("Usna_NUmber",fid);
                intent.putExtra("coming_from_Fstudent","100");
                startActivity(intent);



            }
        });
    }
















    class AsyncLogin extends AsyncTask<String,String,String>
    {
        private Activity activity;
        //Context context;
        ProgressDialog pd = new ProgressDialog(DeleteNotice.this);
        HttpURLConnection conn;
        URL url=null;
        //public boolean x=false;
        public AsyncLogin(Activity activity) {
            this.activity = activity;
        }
        protected void onPreExecute()
        {
            pd.setMessage( "\t Please Wait !! Loading..." );
            pd.setCancelable( false );
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try{
                url=new URL( "http://counsellingmanagementsystem.000webhostapp.com/deleteallnotice.php");
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception";
            }
            try{
                conn=(HttpURLConnection)url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(35000);
                conn.setRequestMethod("POST");
                conn.setDoInput( true );
                conn.setDoOutput( true );
                Uri.Builder builder;
                builder= new Uri.Builder().appendQueryParameter("fid",params[0]);
                String query= builder.build().getEncodedQuery();
                OutputStream os=conn.getOutputStream();
                BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(os,"UTF8"));
                writer.write( query );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : "+e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader( new InputStreamReader( input )
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append( line );
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            }
            catch (IOException e){
                e.printStackTrace();
                return ("Exception : "+e.getMessage());
            }
            finally {
                conn.disconnect();
            }
        }
        protected void onPostExecute(String result)
        {
            pd.dismiss();
            Toast.makeText(DeleteNotice.this,result, Toast.LENGTH_SHORT ).show();
            //res=result.toString();
            if(result.equalsIgnoreCase( "Login Successful" )) {
                // viewStatus.setText(result.toString());
                //Toast.makeText(Login.this, "success", Toast.LENGTH_SHORT).show();
                activity.startActivity( new Intent( activity,MainActivity.class ) );
                Intent intent1=new Intent( getApplicationContext(),Studentgrid.class );
                String varMsg="Welcome to Main Menu, Please Select Your Desired Service.";
                intent1.putExtra( "msg", varMsg);
                startActivity( intent1 );
 /* Intent intent = new Intent(MainActivity.this, MenuActivity.class);
 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 MainActivity.this.startActivity(intent);*/
                // Intent MenuIntent=new Intent("android.intent.action.MenuActivity.class");
                // startActivity( MenuIntent );
            }
            else if(result.equalsIgnoreCase( "Login Failed" ))
            {
                Toast.makeText(DeleteNotice.this, "Login Failed", Toast.LENGTH_SHORT
                ).show();
            }
        }
    }







}
