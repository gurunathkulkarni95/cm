package com.example.pooja.cm;

/**
 * Created by gurunath on 24/05/18.
 */


public class Dnoticeproduct {
    private String fid;
    private String message;
    private String imagepath;

    public Dnoticeproduct(String fid, String message,String imagepath) {
        this.fid = fid;
        this.message = message;
        this.imagepath = imagepath;
    }


    public String getFid() {
        return fid;
    }

    public String getMessage() {
        return message;
    }

    public String getImagepath() {
        return imagepath;
    }
}
