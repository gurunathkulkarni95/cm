package com.example.pooja.cm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

import static com.example.pooja.cm.R.id.grid;

public class Admingrid extends AppCompatActivity {
    GridLayout maingrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admingrid);


        maingrid = (GridLayout) findViewById(grid);
        setSingleEvent(maingrid);
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                     if (choice == 0) {
                        Intent a = new Intent(Admingrid.this, Facultyregister.class);
                        startActivity(a);
                    } else if (choice == 1) {
                        Intent a = new Intent(Admingrid.this, Subjectregisteration.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Admingrid.this, Studentallotment.class);
                        startActivity(a);
                    }  else if (choice == 3) {
                     Intent a = new Intent(Admingrid.this, Teaches.class);
                     startActivity(a);
                     }
                     else if (choice == 4) {
                         Intent a = new Intent(Admingrid.this, Adminstudpassword.class);
                         startActivity(a);
                     }
                     else if (choice == 5) {
                         logoutOperationa();
                     }
                }
            });
        }
    }

    private void logoutOperationa() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Admingrid.this);
        builder1.setMessage("Do you want to logout now ?");
        builder1.setCancelable(true);
        builder1.setTitle("Log Out");

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent intent = new Intent(Admingrid.this, MainActivity
                                .class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                });

        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog logOutDialog = builder1.create();
        logOutDialog.show();

    }
    @Override
    public void onBackPressed() {
        logoutOperationa();
    }
}

