package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class Cgpadispadapter extends RecyclerView.Adapter<Cgpadispadapter.ProductViewHolder> {
Config config;
String sem;

    private Context mCtx;
    private List<Cgpaproduct> productList;

    public Cgpadispadapter(Context mCtx, List<Cgpaproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.cgpalist, null);
        return new ProductViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Cgpaproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(product.getSgpa());
        holder.t2.setText(product.getCgpa());
        holder.t3.setText("CGPA UPTO "+sem+"Semester");

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7;


        public ProductViewHolder(View itemView) {
            super(itemView);
            sem=config.sem;
            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3=itemView.findViewById(R.id.t3);

            //imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
