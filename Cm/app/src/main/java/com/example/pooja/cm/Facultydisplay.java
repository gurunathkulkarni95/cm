package com.example.pooja.cm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Facultydisplay extends AppCompatActivity {
    String fid;
    Config config=new Config();
    FacultyDisplayModel facultyDisplayModel;
    EditText e1;
    Button b1;
    //this is the JSON Data URL
    //make sure you are using the correct ip else it will not work
    private static final String URL_PRODUCTS = "http://counsellingmanagementsystem.000webhostapp.com/jsonfaculty.php";

    //a list to store all the products
    List<FacultyProduct> productList;

    //the recyclerview
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultydisplay);
        fid=config.fid;

        b1 = (Button) findViewById(R.id.submit);
       // e1 = (EditText) findViewById(R.id.us);
        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.recylcerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initializing the productlist
        productList = new ArrayList<>();

        //this method will fetch and parse json
        //to display it in recyclerview
        //Toast.makeText(getApplicationContext(),fid,Toast.LENGTH_SHORT).show();
        loadProducts();



    }

    private void loadProducts() {

        /*
        * Creating a String Request
        * The request type is GET defined by first parameter
        * The URL is defined in the second parameter
        * Then we have a Response Listener and a Error Listener
        * In response listener we will get the JSON response as a String
        * */
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        Gson gsonLoginInfo = new Gson();
//Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();
                        facultyDisplayModel = gsonLoginInfo.fromJson(response, FacultyDisplayModel.class);
                        //converting the string to json array object
                        //JSONArray array = new JSONArray(response);
                        //traversing through all the object
                            /*for (int i = 0; i < array.length(); i++) {

                                //getting product object from json array
                                JSONObject product = array.getJSONObject(i);

                                //adding the product to product list
                                productList.add(new Product(
                                        product.getString("firstname"),
                                        product.getString("midname"),
                                        product.getString("lastname"),
                                        product.getString("usn"),
                                        product.getString("cetranking"),
                                        product.getString("course"),
                                        product.getString("yearofadmission"),
                                        product.getString("contactno"),
                                        product.getString("date"),
                                        product.getString("mode"),
                                        product.getString("email"),
                                        product.getString("drivinglicense"),
                                        product.getString("passport"),
                                        product.getString("aadharno"),
                                        product.getString("bloodgroup"),
                                        product.getString("fathername"),
                                        product.getString("fatheroccupation"),
                                        product.getString("fathercontact"),
                                        product.getString("mothername"),
                                        product.getString("motheroccupation"),
                                        product.getString("mothercontact"),
                                        product.getString("csn"),
                                        product.getString("residential")
                                ));
                            }*/

                        //creating adapter object and setting it to recyclerview
                        Facultydispadapter adapter = new Facultydispadapter(Facultydisplay.this, facultyDisplayModel);
                        recyclerView.setAdapter(adapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fid",fid);

                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }

}

