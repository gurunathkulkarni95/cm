package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Login extends AppCompatActivity {
    EditText e1,e2;
    TextView t;
Button b;
    Config config=new Config();
    String usn,Password,res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        e1=(EditText)findViewById(R.id.id2);
        e2=(EditText)findViewById(R.id.b);
        b=(Button)findViewById(R.id.click);
        t=(TextView)findViewById(R.id.id6);


        final Intent intent=getIntent();
        String varConfirmation = intent.getStringExtra( "confirmation" );
        //Toast.makeText( this, varConfirmation, Toast.LENGTH_SHORT ).show();
        t.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String varMsg="Welcome to Registration Page";
                Intent intent=new Intent(Login.this,RegistrationActivity.class);
                intent.putExtra( "message",varMsg );
                startActivity( intent );
            }
        } );
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usn=e1.getText().toString();
                config.usn=usn;
                Password=e2.getText().toString();
                AsyncLogin login=new AsyncLogin(Login.this);
                login.execute(usn,Password);
                //Intent intent1 = new Intent( getApplicationContext(), MenuActivity.class );
                // startActivity( intent1 );
               // Toast.makeText( Login.this, res, Toast.LENGTH_SHORT ).show();
            }
        });
    }
    class AsyncLogin extends AsyncTask<String,String,String>
    {
        private Activity activity;
        //Context context;
        ProgressDialog pd = new ProgressDialog(Login.this);
        HttpURLConnection conn;
        URL url=null;
        //public boolean x=false;
        public AsyncLogin(Activity activity) {
            this.activity = activity;
        }
        protected void onPreExecute()
        {
            pd.setMessage( "\t Please Wait !! Loading..." );
            pd.setCancelable( false );
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try{
                url=new URL( "http://counsellingmanagementsystem.000webhostapp.com/custlogin.php");
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception";
            }
            try{
                conn=(HttpURLConnection)url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(35000);
                conn.setRequestMethod("POST");
                conn.setDoInput( true );
                conn.setDoOutput( true );
                Uri.Builder builder;
                builder= new Uri.Builder().appendQueryParameter("usn",params[0]);
                builder.appendQueryParameter("password",params[1]);
                String query= builder.build().getEncodedQuery();
                OutputStream os=conn.getOutputStream();
                BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(os,"UTF8"));
                writer.write( query );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : "+e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader( new InputStreamReader( input )
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append( line );
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            }
            catch (IOException e){
                e.printStackTrace();
                return ("Exception : "+e.getMessage());
            }
            finally {
                conn.disconnect();
            }
        }
        protected void onPostExecute(String result)
        {
            pd.dismiss();
            Toast.makeText(Login.this,result, Toast.LENGTH_SHORT ).show();
            //res=result.toString();
            if(result.equalsIgnoreCase( "Login Successful" )) {
                // viewStatus.setText(result.toString());
                //Toast.makeText(Login.this, "success", Toast.LENGTH_SHORT).show();
                activity.startActivity( new Intent( activity,MainActivity.class ) );
                Intent intent1=new Intent( getApplicationContext(),Studentgrid.class );
                String varMsg="Welcome to Main Menu, Please Select Your Desired Service.";
                intent1.putExtra( "msg", varMsg);
                startActivity( intent1 );
 /* Intent intent = new Intent(MainActivity.this, MenuActivity.class);
 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 MainActivity.this.startActivity(intent);*/
                // Intent MenuIntent=new Intent("android.intent.action.MenuActivity.class");
                // startActivity( MenuIntent );
            }
            else if(result.equalsIgnoreCase( "Login Failed" ))
            {
                Toast.makeText(Login.this, "Login Failed", Toast.LENGTH_SHORT
                ).show();
            }
        }
    }
}
