package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Sgpacal extends AppCompatActivity {


    Spinner sem;
    String usn;
    public  String mode;
    Config config;
    TextView u,semester;
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sgpacal);
        sem = (Spinner) findViewById(R.id.sem);
        u=(TextView)findViewById(R.id.usn);
        semester=(TextView)findViewById(R.id.s);
        b1=(Button)findViewById(R.id.b1);
        sem.setPrompt("semester");
        config = new Config();
        String s = getIntent().getStringExtra("coming_from_Fstudent");
        if (s != null) {
            if (s.equalsIgnoreCase("100")) {
                usn = getIntent().getStringExtra("Usna_NUmber");
            }
        } else {
            usn = config.usn;
        }

        //Toast.makeText(getApplicationContext(), usn, Toast.LENGTH_SHORT).show();
        u.setText(usn);



        ArrayAdapter adapter1=ArrayAdapter.createFromResource(this,R.array.des,android.R.layout.simple_spinner_item);
        sem.setAdapter(adapter1);

        sem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mode=sem.getSelectedItem().toString();
                semester.setText(mode);
                config.sem=mode;
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });


        Toast.makeText(getApplicationContext(), mode,Toast.LENGTH_SHORT).show();
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AsyncReg reg = new AsyncReg(Sgpacal.this);
                reg.execute(usn,mode);


            }
        });



    }


    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Sgpacal.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/sgpa.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("sem", params[1]);


                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            Intent i=new Intent(getApplicationContext(),Cgpadispaly.class);
            i.putExtra("Usna_NUmber",usn);
            i.putExtra("sem",mode);
            i.putExtra("coming_from_Fstudent","100");
            startActivity(i);

            }


        }

    }

