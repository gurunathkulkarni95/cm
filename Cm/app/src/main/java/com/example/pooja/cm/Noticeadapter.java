package com.example.pooja.cm;

/**
 * Created by gurunath on 05/04/18.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


public class Noticeadapter extends RecyclerView.Adapter<Noticeadapter.ProductViewHolder> {

Config config;
    private Context mCtx;
    private List<Noticeproduct> productList;

    public Noticeadapter(Context mCtx, List<Noticeproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.productlist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Noticeproduct product = productList.get(position);
config=new Config();
        //loading the image
        Glide.with(mCtx)
                .load(product.getImage())
                .into(holder.imageView);

        Glide.with(mCtx)
                .load(product.getImage1())
                .into(holder.imageView1);
        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.imageView=holder.imageView1;
                Intent intent=new Intent(mCtx,Noticepic.class);
                mCtx.startActivity(intent);
                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        config.imageView=holder.imageView;
                        Intent intent=new Intent(mCtx,Noticezoom.class);
                        mCtx.startActivity(intent);

                    }
                });



            }
        });


        holder.textViewTitle.setText(product.getMessage());
        holder.textViewShortDesc.setText(product.getFname());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc, textViewRating, textViewPrice;
        ImageView imageView,imageView1;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewShortDesc = itemView.findViewById(R.id.textViewShortDesc);
            imageView1 = itemView.findViewById(R.id.image);
            imageView = itemView.findViewById(R.id.imageView);


        }
    }
}

