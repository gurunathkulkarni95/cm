package com.example.pooja.cm;

/**
 * Created by gurunath on 12/04/18.
 */

public class Cnoticeproduct {
    private String fname;
    private String message;
    private String imagepath;

    public Cnoticeproduct(String fname, String message,String imagepath) {
        this.fname = fname;
        this.message = message;
        this.imagepath = imagepath;
    }


    public String getFname() {
        return fname;
    }

    public String getMessage() {
        return message;
    }

    public String getImagepath() {
        return imagepath;
    }
}

