package com.example.pooja.cm;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class Scholarshipdispadapter extends RecyclerView.Adapter<Scholarshipdispadapter.ProductViewHolder> {


    private Context mCtx;
    private List<Scholarshipproduct> productList;

    public Scholarshipdispadapter(Context mCtx, List<Scholarshipproduct> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.scholarshiplist, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Scholarshipproduct product = productList.get(position);

        //loading the image
        /** Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(product.getUsn());
        holder.t2.setText(product.getName());
        holder.t3.setText(product.getOrganization());
        holder.t4.setText(product.getDate());
        holder.t5.setText(product.getAmount());


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3 = itemView.findViewById(R.id.t3);
            t4 = itemView.findViewById(R.id.t4);
            t5 = itemView.findViewById(R.id.t5);

            //imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
