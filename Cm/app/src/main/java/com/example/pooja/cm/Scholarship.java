package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Scholarship extends AppCompatActivity  {
    String usn;
    public String mode;
    EditText org,date,amount;
    Spinner name;
    Config config=new Config();
    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholarship);
        org=(EditText)findViewById(R.id.org);
        date=(EditText)findViewById(R.id.datemonth);
        amount=(EditText)findViewById(R.id.amount);
        name=(Spinner)findViewById(R.id.name);
        b1=(Button)findViewById(R.id.b1);
        b2=(Button)findViewById(R.id.b2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                org.setText("");
                date.setText("");
                amount.setText("");

            }
        });
        name.setPrompt("Scholarship type");
        ArrayAdapter adapter1=ArrayAdapter.createFromResource(this,R.array.name,android.R.layout.simple_spinner_item);
        name.setAdapter(adapter1);
        name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:mode="epass";
                        break;
                    case 1:mode="national portal";
                        break;
                    case 2:mode="shri c.v.raman";
                        break;
                    case 3:mode="post matric";
                        break;

                    case 4:mode="national merit";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncReg reg = new AsyncReg(Scholarship.this);
                usn=config.usn;
                reg.execute(usn,mode,org.getText().toString(),date.getText().toString(),amount.getText().toString());
                Intent intent = new Intent(getApplicationContext(), Studentgrid.class);
                startActivity(intent);
            }
        });


    }
    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Scholarship.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/scholarship.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);
                builder.appendQueryParameter("mode", params[1]);
                builder.appendQueryParameter("org", params[2]);
                builder.appendQueryParameter("date", params[3]);
                builder.appendQueryParameter("amount", params[4]);
                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Scholarship.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully form filled!!")) {
                Toast.makeText(Scholarship.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Facultygrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Scholarship.this, "form fill up Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

