package com.example.pooja.cm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Belal on 10/18/2017.
 */

public class StudentdispAdapter extends RecyclerView.Adapter<StudentdispAdapter.ProductViewHolder> {


    private Context mCtx;
    private List<Product> productList;
    StudentDisplayModel model;

    public StudentdispAdapter(Context mCtx, StudentDisplayModel model) {
        this.mCtx = mCtx;
        this.model = model;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.studentdisp_list, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
      //  Product product = productList.get(position);

        //loading the image
        /**Glide.with(mCtx)
         .load(product.getImage())
         .into(holder.imageView);**/

        holder.t1.setText(model.getFirstname());
        holder.t2.setText(model.getMidname());
        holder.t3.setText(model.getLastname());
        holder.t4.setText(model.getUsn());
        holder.t5.setText(model.getCetranking());
        holder.t6.setText(model.getCourse());
        holder.t7.setText(model.getYearofadmission());
        holder.t8.setText(model.getContactno());
        holder.t9.setText(String.valueOf(model.getDateofbirth()));
        holder.t10.setText(model.getMode());
        holder.t11.setText(model.getEmail());
        holder.t12.setText(model.getDrivinglicense());
        holder.t13.setText(model.getPassport());
        holder.t14.setText(model.getAadharno());
        holder.t15.setText(model.getBloodgroup());
        holder.t16.setText(model.getFathername());
        holder.t17.setText(model.getFatheroccupation());
        holder.t18.setText(model.getFathercontact());
        holder.t19.setText(model.getMothername());
        holder.t20.setText(model.getMotheroccupation());
        holder.t21.setText(model.getMothercontact());
        holder.t22.setText(model.getCsn());
        holder.t23.setText(model.getResidential());
       // holder.imageView.setText(model.getImagepath());

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23;
        TextView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            t3= itemView.findViewById(R.id.t3);
            t4= itemView.findViewById(R.id.t4);
            t5= itemView.findViewById(R.id.t5);
            t6 = itemView.findViewById(R.id.t6);
            t7 = itemView.findViewById(R.id.t7);
            t8= itemView.findViewById(R.id.t8);
            t9= itemView.findViewById(R.id.t9);
            t10= itemView.findViewById(R.id.t10);
            t11= itemView.findViewById(R.id.t11);
            t12= itemView.findViewById(R.id.t12);
            t13= itemView.findViewById(R.id.t13);
            t14= itemView.findViewById(R.id.t14);
            t15= itemView.findViewById(R.id.t15);
            t16 = itemView.findViewById(R.id.t16);
            t17 = itemView.findViewById(R.id.t17);
            t18= itemView.findViewById(R.id.t18);
            t19 = itemView.findViewById(R.id.t19);
            t20= itemView.findViewById(R.id.t20);
            t21= itemView.findViewById(R.id.t21);
            t22= itemView.findViewById(R.id.t22);
            t23= itemView.findViewById(R.id.t23);
            //imageView=itemView.findViewById(R.id.image);


        }
    }
}

