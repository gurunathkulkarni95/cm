package com.example.pooja.cm;


public class Marksproduct {
    private String usn,subjectcode,sem,year,cie1,cie2,cie3,compensatory,assignment,grade,credit;


    public Marksproduct(String usn, String subjectcode, String sem, String year,String cie1, String cie2, String cie3, String compensatory,String assignment, String grade, String credit) {
        this.usn= usn;
        this.subjectcode = subjectcode;
        this.sem=sem;
        this.year= year;
        this.cie1= cie1;
        this.cie2=cie2;
        this.cie3=cie3;
        this.compensatory= compensatory;
        this.assignment = assignment;
        this.grade=grade;
        this.credit=credit;
    }

    public String getUsn() {
        return usn;
    }

    public String getSubjectcode() {
        return subjectcode;
    }

    public String getSem() {
        return sem;
    }

    public String getYear() {
        return year;
    }
    public String getCie1() {
        return cie1;
    }

    public String getCie2() {
        return cie2;
    }

    public String getCie3() {
        return cie3;
    }

    public String getCompensatory() {
        return compensatory;
    }
    public String getAssignment() {
        return assignment;
    }

    public String getGrade() {
        return grade;
    }

    public String getCredit() {
        return credit;
    }



}

