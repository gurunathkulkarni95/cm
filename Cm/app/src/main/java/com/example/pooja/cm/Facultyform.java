package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Facultyform extends AppCompatActivity  {

    public String mode,mode1,mode2;
EditText fn,fid,email,desig,contact;
    EditText sem,subject,subcode;

    Button b1,clear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultyform);
        fn=(EditText)findViewById(R.id.c1);
        fid=(EditText)findViewById(R.id.c2);
        contact=(EditText)findViewById(R.id.c3);
        email=(EditText)findViewById(R.id.c5);
        desig=(EditText)findViewById(R.id.c4);
        sem=(EditText) findViewById(R.id.sem);
        subject=(EditText) findViewById(R.id.subject);
        subcode=(EditText)findViewById(R.id.subcode);
        b1=(Button)findViewById(R.id.submit);
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fn.setText("");
                fid.setText("");
                email.setText("");
                desig.setText("");
                contact.setText("");

            }
        });





        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncReg reg = new AsyncReg(Facultyform.this);

                reg.execute(fn.getText().toString(), fid.getText().toString(), desig.getText().toString(),email.getText().toString(),contact.getText().toString(),subject.getText().toString(),subcode.getText().toString(),sem.getText().toString());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }
    class AsyncReg extends AsyncTask<String, String, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Facultyform.this);
        HttpURLConnection conn;
        URL url = null;

        public AsyncReg(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/facultyform.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("fn", params[0]);
                builder.appendQueryParameter("fid", params[1]);
                builder.appendQueryParameter("desig", params[2]);
                builder.appendQueryParameter("email", params[3]);
                builder.appendQueryParameter("contact", params[4]);
                builder.appendQueryParameter("mode1", params[5]);
                builder.appendQueryParameter("mode2", params[6]);
                builder.appendQueryParameter("mode", params[7]);

                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            Toast.makeText(Facultyform.this, result, Toast.LENGTH_SHORT).show();
            if (result.equalsIgnoreCase("Successfully form filled!!")) {
                Toast.makeText(Facultyform.this, "Registration Successful !!",
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, Facultygrid.class));
 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
            } else if (result.equalsIgnoreCase("Try again please")) {
                Toast.makeText(Facultyform.this, "form fill up Failed. Try Again",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

