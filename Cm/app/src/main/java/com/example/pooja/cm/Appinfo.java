package com.example.pooja.cm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

public class Appinfo extends AppCompatActivity {

    GridLayout maingrid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appinfo);
        maingrid=(GridLayout)findViewById(R.id.grid);
        setSingleEvent(maingrid);
    }

    private void setSingleEvent(GridLayout maingrid) {
        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(Appinfo.this, Aboutus.class);
                        startActivity(a);
                    } else if (choice == 1) {
                        Intent a = new Intent(Appinfo.this, Contactus.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Appinfo.this, Help.class);
                        startActivity(a);
                    }


                }
            });
        }
    }


       // public void onBackPressed () {
         //   Intent a = new Intent(Intent.ACTION_MAIN);
           // a.addCategory(Intent.CATEGORY_HOME);
            //a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //startActivity(a);
        //}

}



