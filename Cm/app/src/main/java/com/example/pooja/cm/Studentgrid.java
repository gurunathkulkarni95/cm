package com.example.pooja.cm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Studentgrid extends AppCompatActivity {
    GridLayout maingrid;
    Bitmap myBitmap,bitmap1,b1;
    ImageView image;
    String usn1,s1;
    TextView t1;
    Config config=new Config();
    String serverurl="http://counsellingmanagementsystem.000webhostapp.com/fetchimage.php";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentgrid);


        maingrid = (GridLayout) findViewById(R.id.grid);
        image=(ImageView)findViewById(R.id.image);
        t1=(TextView)findViewById(R.id.usn);

        usn1=config.usn;
        t1.setText(usn1);
        // Toast.makeText(getApplicationContext(),usn1,Toast.LENGTH_SHORT).show();
        // getImage();
        //getImage();
        Downloadinamge downloadinamge=new Downloadinamge(Studentgrid.this);
        downloadinamge.execute(usn1);
        setSingleEvent(maingrid);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ByteArrayOutputStream stream = new ByteArrayOutputStream();
                //bitmap1.compress(Bitmap.CompressFormat.PNG, 50, stream);
                //byte[] byteArray = stream.toByteArray();
                config.b=bitmap1;
                Intent i=new Intent(Studentgrid.this,Studentimagedisplay.class);

                //i.putExtra("image",byteArray);
                startActivity(i);
            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSingleEvent(GridLayout maingrid) {


        for (int i = 0; i < maingrid.getChildCount(); i++) {
            CardView cardView = (CardView) maingrid.getChildAt(i);

            final int choice = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (choice == 0) {
                        Intent a = new Intent(Studentgrid.this, Studentform.class);
                        startActivity(a);
                    } else if (choice == 1) {
                        Intent a = new Intent(Studentgrid.this, Sstudentgrid.class);
                        startActivity(a);
                    } else if (choice == 2) {
                        Intent a = new Intent(Studentgrid.this, Previousrecord.class);
                        startActivity(a);
                    } else if (choice == 3) {
                        Intent a = new Intent(Studentgrid.this, ActivityGrid.class);
                        startActivity(a);
                    }  else if (choice == 4) {
                        Intent a = new Intent(Studentgrid.this, Marksentryinst.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 5) {
                        Intent a = new Intent(Studentgrid.this, Upload_student.class);
                        startActivity(a);
                    }
                    else if (choice == 6) {
                        Intent a = new Intent(Studentgrid.this, Cnoticedisplay.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 7) {
                        Intent a = new Intent(Studentgrid.this, Changestudpassword.class);
                        a.putExtra("Usna_NUmber",usn1);
                        a.putExtra("coming_from_Fstudent","100");
                        startActivity(a);
                    }
                    else if (choice == 8) {
                        logoutOperationa();
                    }
                }
            });
        }
    }




   /* void getImage()
    {

        ImageRequest imageRequest=new ImageRequest(serverurl,  new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
image.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_SHORT).show();
            }
        });
        MySingleton.getInstance(Studentgrid.this).addToRequestQueue(imageRequest);
    }*/




    class  Downloadinamge extends AsyncTask<String, Void, String> {
        private Activity activity;
        String varConfirmation = "Welcome to Login page. Please Login !!";
        ProgressDialog pd = new ProgressDialog(Studentgrid.this);
        HttpURLConnection conn;
        URL url = null;

        public Downloadinamge(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/fetchimage.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                // return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("usn", params[0]);




                String query = builder.build().getEncodedQuery();
                //String query1=;
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                // writer.write( query1 );
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    myBitmap = BitmapFactory.decodeStream(input);
                    s1=BitMapToString(myBitmap);





                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());


                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }




        }

        protected void onPostExecute(String result) {
            pd.dismiss();

            bitmap1=StringToBitMap(s1);

            image.setImageBitmap(bitmap1);

 /*Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
 intent.putExtra( "confirmation", varConfirmation);
 startActivity( intent );*/
        }


        public String BitMapToString(Bitmap bitmap){
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,50, baos);
            byte [] b=baos.toByteArray();
            String temp= Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }


        public Bitmap StringToBitMap(String encodedString){
            try {
                byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
                Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch(Exception e) {
                e.getMessage();
                return null;
            }
        }


    }





    private void logoutOperationa() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Studentgrid.this);
        builder1.setMessage("Do you want to logout now ?");
        builder1.setCancelable(true);
        builder1.setTitle("Log Out");

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent intent = new Intent(Studentgrid.this, MainActivity
                                .class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                });

        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog logOutDialog = builder1.create();
        logOutDialog.show();

    }


    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();

        logoutOperationa();

        //startActivity(new Intent(getApplicationContext(), Parentgrid.class));
        //finish();

    }

}






