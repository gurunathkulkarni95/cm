package com.example.pooja.cm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Counsellorlogin extends AppCompatActivity {
    EditText e1, e2;
    TextView t;
    Button b;
    String Username, Password, res;
    Config config = new Config();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counsellorlogin);

        e1 = (EditText) findViewById(R.id.id2);
        e2 = (EditText) findViewById(R.id.b);
        b = (Button) findViewById(R.id.click);
        t = (TextView) findViewById(R.id.id6);


        final Intent intent = getIntent();
        String varConfirmation = intent.getStringExtra("confirmation");
        //Toast.makeText( this, varConfirmation, Toast.LENGTH_SHORT ).show();
        /** t.setOnClickListener( new View.OnClickListener() {
        @Override public void onClick(View v) {
        String varMsg="Welcome to Registration Page";
        Intent intent=new Intent(Counsellorlogin.this,Counsellorregister.class);
        intent.putExtra( "message",varMsg );
        startActivity( intent );
        }
        } );**/
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Username = e1.getText().toString();
                Password = e2.getText().toString();
                config.fid = Username;
                AsyncLogin login = new AsyncLogin(Counsellorlogin.this);
                login.execute(Username, Password);
                //Intent intent1 = new Intent( getApplicationContext(), MenuActivity.class );
                // startActivity( intent1 );
                //Toast.makeText(Counsellorlogin.this, res, Toast.LENGTH_SHORT ).show();
            }
        });
    }

    class AsyncLogin extends AsyncTask<String, String, String> {
        private Activity activity;
        //Context context;
        ProgressDialog pd = new ProgressDialog(Counsellorlogin.this);
        HttpURLConnection conn;
        URL url = null;

        //public boolean x=false;
        public AsyncLogin(Activity activity) {
            this.activity = activity;
        }

        protected void onPreExecute() {
            pd.setMessage("\t Please Wait !! Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                url = new URL("http://counsellingmanagementsystem.000webhostapp.com/faclogin.php");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder;
                builder = new Uri.Builder().appendQueryParameter("userID", params[0]);
                builder.appendQueryParameter("password", params[1]);
                String query = builder.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception : " + e.getMessage();
            }
            try {
                int response_code = conn.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input)
                    );
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null)
                        result.append(line);
                    return (result.toString());
                } else {
                    return ("Unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return ("Exception : " + e.getMessage());
            } finally {
                conn.disconnect();
            }
        }

        protected void onPostExecute(String result) {
            pd.dismiss();
            //Toast.makeText(Parent_login.this, result, Toast.LENGTH_SHORT).show();
            //Intent i = new Intent(Parent_login.this, ParentGrid.class);

            //startActivity(i);

            //res=result.toString();
            if (result.equalsIgnoreCase("Login Successful")) {
                //viewStatus.setText(result.toString());
                //Toast.makeText(Parent_login.this, "success", Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, MainActivity.class));
                Intent intent1 = new Intent(getApplicationContext(), Counsellorgrid.class);
                intent1.putExtra("Usna_NUmber", e1.getText().toString());
                intent1.putExtra("coming_from_Fstudent", "100");
                String varMsg = "Welcome to Main Menu, Please Select Your Desired Service.";
                intent1.putExtra("msg", varMsg);
                startActivity(intent1);
            }
            else
            {
                Toast.makeText(Counsellorlogin.this, "unsuccessful", Toast.LENGTH_SHORT).show();
            }


        }
    }
}
